<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Source\Controllers\Pesquisa;
use Source\Controllers\RespostaPesquisa;
use Source\Controllers\CriarPesquisa;
use Source\Controllers\BuscarPesquisa;
use Source\Controllers\CriarUsuario;
use Source\Middleware\AuthBasica;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/source/Infrastructure/DAO/bootstrap.php';
require __DIR__ . "/source/Infrastructure/Middleware/AuthBasica.php";
require __DIR__ . '/source/Domain/Models/Pesquisa.php';
require __DIR__ . '/source/Domain/Models/Usuario.php';
require __DIR__ . '/source/Domain/Models/Resposta.php';
require __DIR__ . '/source/Domain/Models/Validacao.php';
require __DIR__ . '/source/Domain/Controllers/Pesquisas.php';
require __DIR__ . '/source/Domain/Controllers/Respostas.php';
require __DIR__ . '/source/Domain/Controllers/Usuarios.php';
require __DIR__ . '/source/Domain/Controllers/Token.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

$app->get('/login', function (Request $request, Response $response) {

   \Source\Middleware\AuthBasica::login();
   //\Source\Middleware\AuthBasica::auth();
    
});


//$app->group('', function () use ($app) {


    $app->post('/usuario', function (Request $request, Response $response) {

        \Source\Controllers\Usuarios::CriarUsuario();

    });

    $app->get('/usuario', function (Request $request, Response $response) {

        \Source\Controllers\Usuarios::BuscarUsuario();

    });

    $app->post('/', function (Request $request, Response $response) {

        \Source\Controllers\Pesquisas::CriarPesquisa();

    });

    $app->get('/', function (Request $request, Response $response) {

        \Source\Controllers\Pesquisas::BuscarPesquisa();

    });

    $app->put('/', function (Request $request, Response $response) {

        \Source\Controllers\Pesquisas::AtualizarPesquisa();

    });

    $app->delete('/', function (Request $request, Response $response) {

        \Source\Controllers\Pesquisas::DeletarPesquisa();

    });

    $app->post('/resposta', function (Request $request, Response $response) {

        \Source\Controllers\Respostas::CriarResposta();

    });

    $app->get('/resposta', function (Request $request, Response $response) {

        \Source\Controllers\Respostas::BuscarResposta();

    });


//})->add(AuthBasica::auth());

$app->run();



