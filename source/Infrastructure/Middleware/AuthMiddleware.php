<?php
namespace Source\Middlewares;

class AuthMiddleware extends Middleware{
    public function __invoke($requisicao, $resposta, $prox){
    

        if(!$this->container->auth->check()){
            $this->container->flash->addMessage('Faça autenticacao');
            return $response->withRedirect($this->container->router->pathFor('/'));
        }

        $resposta = $prox($requisicao, $resposta);
        return $resposta;
    }



}
