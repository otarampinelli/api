<?php 


namespace Source\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Resposta extends Eloquent {

    public $timestamps = false;
    protected $table = 'resposta';
    protected $fillable = [

        'resposta'

    ];

};