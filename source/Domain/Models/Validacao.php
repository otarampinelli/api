<?php 

namespace Source\Models;

final class Validacao {
    public static function validacaoString(string $String) {
        return strlen($String) >= 3 && !is_numeric($String);
    }

    public static function validacaoInteger(string $Integer) {
        return filter_var($Integer, FILTER_VALIDATE_INT) && $Integer > 0;
    }

    public static function validacaoBool($Bool) {
        return filter_input(FILTER_VALIDATE_BOOLEAN, $Bool);
    }

    public static function validacaoEmail(string $Email) {
        return filter_var($Email, FILTER_VALIDATE_EMAIL);
    }
}
