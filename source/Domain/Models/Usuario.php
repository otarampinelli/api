<?php

namespace Source\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Usuario extends Eloquent {

    public $timestamps = false;
    protected $table = 'usuario';
    protected $fillable = [

        'usuario', 'senha', 'email', 'token'

    ];

};
