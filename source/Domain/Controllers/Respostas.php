<?php

namespace Source\Controllers;

use Source\Models\Resposta;
use Source\Models\Validacao;

class Respostas {

    static function CriarResposta() {

            $dados = json_decode(file_get_contents("php://input"),false);

            if(!$dados){
                header("HTTP/1.1 400 Bad Request");
                echo json_encode(array("response"=>"Nenhum dado inserido!"));
                exit;
            }

            $erros = array();

            if(!Validacao::validacaoBool($dados->resposta) !== true || false) {
                array_push($erros,"Resposta");
            }

            if(count($erros) > 0) {
                header("HTTP/1.1 400 Bad Request");
                echo json_encode(array("resposta"=>"Há campos invalidos no formulario!", "fields"=>$erros)); 
                exit;
            }

            $resposta = new Resposta();
            $resposta->resposta = $dados->resposta;
            $resposta->save();
            
            header("HTTP/1.1 201 Created");
            echo json_encode(array("response"=> "Resposta enviada com sucesso!"));

    }

    static function BuscarResposta() {


        $resposta = Resposta::all();

        if($resposta) {

            $buscar = array();

            foreach($resposta as $resposta) {
                array_push($buscar, $resposta);
            }

            echo json_encode(array("response" => $buscar));

        } else {

            echo json_encode(array("response" => "Nenhuma resposta foi localizada!"));

        }

        // $pesquisa = Pesquisa::all();

        // foreach($pesquisa as $pesquisa) {
        //     echo $pesquisa;
        // }

    }

}